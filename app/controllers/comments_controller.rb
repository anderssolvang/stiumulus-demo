class CommentsController < ApplicationController
  before_action :find_post
  include CableReady::Broadcaster

  def create
    comment = Comment.new(comment_params)
    comment.save

    cable_ready["comments"].insert_adjacent_html(
      selector: "#comments-#{params[:post_id]}",
      position: "afterbegin",
      html: render_to_string(partial: "comments/comment", locals: { comment: comment})
    )
    cable_ready.broadcast

    redirect_to posts_path
  end

  private

    def find_post
      @post = Post.find_by_id(params[:comment][:post_id])
    end

    def comment_params
      params.require(:comment).permit(:body, :post_id)
    end

end
