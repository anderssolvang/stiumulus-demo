class Comment < ApplicationRecord
  validates :body, length: { minimum: 1, maximum: 280 }
  belongs_to :post
end
