class Post < ApplicationRecord
  validates :body, length: { minimum: 1, maximum: 280 }

  has_many :comments, dependent: :destroy

end
