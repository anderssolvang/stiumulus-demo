class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.string :body, null: false
      t.string :username, null: false, default: "commenter"
      t.integer :likes_count, null: false, default: 0
      t.references :post, null: false, foreign_key: true

      t.timestamps
    end
  end
end
